# install nix 
sh <(curl -L https://nixos.org/nix/install) --no-daemon

# install starship biar sat set sat set shell nya
curl -sS https://starship.rs/install.sh | sh

# source nix 
. ~/.nix-profile/etc/profile.d/nix.sh

# install packages
nix-env -iA \
    nixpkgs.neovim \
    nixpkgs.tmux \
    nixpkgs.fzf \
    nixpkgs.ripgrep \
    nixpkgs.bat

# install zsh 
command -v zsh | sudo tee -a /etc/shells

# use zsh as default 
sudo chsh -s $(which zsh) $USER


# bundle zsh plugins 
antibody bundle < zsh_plugins.txt > ~/.zsh_plugins.sh
echo 'source ~/.zsh_plugins.sh' >> ~/.zshrc

# install vim-plug
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

# install nvim configs
mkdir -p ~/.config/nvim
cp init.vim ~/.config/nvim

# install neovim plugins
nvim --headless +PlugInstall +qall


# install python3
curl -sSL https://install.python-poetry.org | python3 -

echo ". ~/.nix-profile/etc/profile.d/nix.sh" >> ~/.bashrc
echo 'eval "$(starship init bash)"' >> ~/.bashrc

echo 'eval "$(starship init zsh)"' >> ~/.zshrc
echo ". ~/.nix-profile/etc/profile.d/nix.sh" >> ~/.zshrc
echo 'export PATH="~/.local/bin:$PATH"' >> ~/.zshrc 

 
# aliases 
echo 'dc="docker-compose"' >> ~/.zshrc
echo 'vim="nvim"' >> ~/.zshrc
echo 'v="vim"' >> ~/.zshrc
echo 'd="docker"' >> ~/.zshrc
echo 'dc="docker-compose"' >> ~/.bashrc
echo 'd="docker"' >> ~/.bashrc
echo 'vim="nvim"' >> ~/.bashrc
echo 'v="vim"' >> ~/.bashrc
